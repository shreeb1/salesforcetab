function SkypeTeamsTabConfiguration() {
  var eventActionProperty = 'action';
  var eventdata = 'data';
  this.tabSettings = {};
  this.channelId = '';
  this.groupId = '';
  this.teamId = '';
  this.chatId = '';
  this.isChat = false;
  var _this = this;

  function processMessage(evt) {
    if (evt) {
      var origin = evt.origin || evt.originalEvent.origin;

      // Reject if origin is not from parent window
      // Reject if we are not hosting this as an iframe
      if ((window.location.ancestorOrigins.length < 1) ||
        (origin != window.location.ancestorOrigins[window.location.ancestorOrigins.length - 1])) {
        return;
      }

      try {
        var evtData = JSON.parse(evt.data);
        if (evtData.action == 'loadConfiguration') {
          _this.tabSettings = evtData.data.settings;
          if (evtData.data.isChat) {
            _this.isChat = true;
            _this.chatId = evtData.data.chatId;
          } else {
            _this.teamId = evtData.data.teamId;
            _this.groupId = evtData.data.groupId;
            _this.channelId = evtData.data.channelId;
          }
        } else if (evtData.action == 'onSave') {
          if (_this.onsave && typeof _this.onsave === 'function') {
            _this.onsave();
          } else {
            _this.done();
          }
        }
      } catch (ex) {
        _this.logError(ex.message);
      }
    }
  }

  function sendMessage(message) {
    // Process only if this is hosted as an IFrame
    if (window.location.ancestorOrigins.length < 1) {
      alert('This functionality is supported only if this page is hosted as an iframe');
    }

    window.parent.postMessage(message, window.location.ancestorOrigins[window.location.ancestorOrigins.length - 1]);
  }

  function createEventData(actionName, data) {
    var jsonData = {};
    jsonData[eventActionProperty] = actionName;
    jsonData[eventdata] = data;
    return JSON.stringify(jsonData);
  }

  this.initialize = function () {
    if (window.addEventListener) {
      // For standards-compliant web browsers
      window.addEventListener("message", processMessage, false);
    } else {
      window.attachEvent("onmessage", processMessage);
    }

    sendMessage(createEventData('initialized', true));
  };

  this.ready = function (readyState) {
    if (readyState) {
      sendMessage(createEventData('save', _this.tabSettings));
    } else {
      sendMessage(createEventData('enableSave', false));
    }
  };

  this.done = function () {
    sendMessage(createEventData('done', _this.tabSettings));
  };

  this.error = function (errorText) {
    sendMessage(createEventData('error', errorText));
  };

  this.logError = function (error) {
    sendMessage(createEventData('logError', error));
  };

  this.logInfo = function (info) {
    sendMessage(createEventData('logInfo', info));
  };

  this.logWarning = function (message) {
    sendMessage(createEventData('logWarning', message));
  };
}
